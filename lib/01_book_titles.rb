class Book
  LOWERCASE_WORDS = [
    "a",
    "and",
    "the",
    "or",
    "if",
    "of",
    "in",
    "an"
  ]
  attr_reader :title
  def title=(title)
    new_arr = []
    title.split(" ").each_with_index do |word, idx|
      if idx == 0
        new_arr << word.capitalize
      elsif LOWERCASE_WORDS.include?(word)
        new_arr << word
      else
        new_arr << word.capitalize
      end
    end
    @title = new_arr.join(" ")
  end
end
