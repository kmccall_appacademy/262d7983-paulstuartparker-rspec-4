class Dictionary
  def initialize
    @entries = {}
  end
  def entries
    @entries
  end

  def add(options)
    if options.is_a?(String)
      @entries[options] = nil
    elsif options.is_a?(Hash)
      @entries.merge!(options)
    end
  end

  def keywords
    @entries.keys.sort
  end

  def include?(word)
    keywords.include?(word)
  end

  def find(word)
    @entries.select { |k, v| k.include?(word) }
  end

  def printable
    to_print = keywords.map do |k|
     %Q{[#{k}] "#{@entries[k]}"}
   end
   to_print.join("\n")
 end

end
