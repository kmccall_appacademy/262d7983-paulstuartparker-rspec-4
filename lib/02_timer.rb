class Timer
  attr_reader :seconds
  def initialize(seconds = 0)
    @seconds = seconds
  end

  def seconds=(num)
    @seconds = num
  end

def format(num)
  if num > 10
    "#{num}"
  else
    "0#{num}"
  end
end

def hours
  seconds.to_i / 3600
end

def minutes
  seconds.to_i % 3600 / 60
end

def seconds_digits
  seconds.to_i % 60
end



  def time_string
    "#{format(hours)}:#{format(minutes)}:#{format(seconds_digits)}"
  end
end
